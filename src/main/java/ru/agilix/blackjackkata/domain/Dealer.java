package ru.agilix.blackjackkata.domain;

import java.util.ArrayList;

public class Dealer {
    private String name;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public boolean canHit(ArrayList<Card> cards) {
        int result = 0;
        boolean wasA = false;
        boolean firstA = false;
        for(Card card : cards) {
            int value = card.getValue();
            if(value == 11) {
                if(!wasA) {
                    wasA = true;
                    firstA = true;
                    result += value;
                } else {
                    if(firstA) {
                        result -= 10;
                        firstA = false;
                    }
                    result += 1;
                }
            } else {
                result += value;
            }
        }

        return result < 17 ;
    }

}
